# Cpu Utilization
resource "aws_cloudwatch_metric_alarm" "cpu_utilization" {
  alarm_name          = "[${var.account_name}] [ec2] [${var.env}] ${var.instance_name} - CPU Utilization"
  comparison_operator = "${var.cpu_utilization_comparison_operator}"
  evaluation_periods  = "${var.cpu_utilization_evaluation_periods}"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "${var.cpu_utilization_period}"
  statistic           = "${var.cpu_utilization_statistic}"
  threshold           = "${var.cpu_utilization_threshold}"
  unit                = "${var.cpu_utilization_unit}"
  alarm_actions       = ["${var.alarm_actions}"]
  dimensions {
    InstanceId = "${var.instance_id}"
  }
}

output "cpu_utilization" {
  value = {
    id = "${aws_cloudwatch_metric_alarm.cpu_utilization.id}"
  }
}
