# Disk Space Utilization
resource "aws_cloudwatch_metric_alarm" "disk_space_utilization" {
  alarm_name          = "[${var.account_name}] [ec2] [${var.env}] ${var.instance_name} - Disk Space Utilization - ${element(var.devices["mount"], count.index)}"
  comparison_operator = "${var.disk_space_utilization_comparison_operator}"
  evaluation_periods  = "${var.disk_space_utilization_evaluation_periods}"
  metric_name         = "DiskSpaceUtilization"
  namespace           = "System/Linux"
  period              = "${var.disk_space_utilization_period}"
  statistic           = "${var.disk_space_utilization_statistic}"
  threshold           = "${var.disk_space_utilization_threshold}"
  unit                = "${var.disk_space_utilization_unit}"
  alarm_actions       = ["${var.alarm_actions}"]
  dimensions {
    Filesystem = "${element(var.devices["dev"], count.index)}"
    InstanceId = "${var.instance_id}"
    MountPath  = "/"
  }

  count = "${length(var.devices)}"
}

output "disk_space_utilization" {
  value = {
    id = "${aws_cloudwatch_metric_alarm.disk_space_utilization.id}"
  }
}
