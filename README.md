# Cloud Watch Alerts

```
module "<MODULE_NAME>" {
  source  = "../"
  account = "${var.account}"

  account_name  = "credibilit-tst"
  env           = "tst"
  instance_id   = "${aws_instance.web.id}"
  instance_name = "Test_CloudWatch"
  alarm_actions = ["${aws_sns_topic.monitoring.arn}"]
  devices       = "${map(
    "dev", list("/dev/sda1"),
    "mount", list("/")
  )}"

  // CPU Utilization
  cpu_utilization_comparison_operator = "GreaterThanOrEqualToThreshold"
  cpu_utilization_evaluation_periods  = 3
  cpu_utilization_period              = 300
  cpu_utilization_statistic           = "Average"
  cpu_utilization_threshold           = 90
  cpu_utilization_unit                = "Count"

  // Disk Space Utilization
  disk_space_utilization_comparison_operator  = "GreaterThanOrEqualToThreshold"
  disk_space_utilization_evaluation_periods   = 3
  disk_space_utilization_period               = 300
  disk_space_utilization_statistic            = "Average"
  disk_space_utilization_threshold            = 90
  disk_space_utilization_unit                 = "Percent"

  // Memory Utilization
  memory_utilization_comparison_operator  = "GreaterThanOrEqualToThreshold"
  memory_utilization_evaluation_periods   = 3
  memory_utilization_period               = 300
  memory_utilization_statistic            = "Average"
  memory_utilization_threshold            = 90
  memory_utilization_unit                 = "Percent"

  // Status Check
  status_check_comparison_operator  = "GreaterThanThreshold"
  status_check_evaluation_periods   = 1
  status_check_period               = 180
  status_check_statistic            = "Maximum"
  status_check_threshold            = 0
  status_check_unit                 = "Count"
}
```
