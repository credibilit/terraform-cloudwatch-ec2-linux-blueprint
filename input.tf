variable "account" {
  description = "The AWS account number"
}

variable "account_name" {
  description = "The AWS account name or alias"
}

variable "env" {
  description = "The environment name"
}

variable "instance_id" {
  description = "The AWS ec2 instance id"
}

variable "instance_name" {
  description = "The AWS ec2 instance name"
}

variable "alarm_actions" {
  type = "list"
  description = "The AWS Cloud Watch alarm actions"
}

variable "devices" {
  type = "map"
  description = "List of devices: dev, mount"
}

// CPU Utilization
variable "cpu_utilization_comparison_operator" {
  default = "GreaterThanOrEqualToThreshold"
}

variable "cpu_utilization_evaluation_periods" {
  default = 3
}

variable "cpu_utilization_period" {
  default = 300
}

variable "cpu_utilization_statistic" {
  default = "Average"
}

variable "cpu_utilization_threshold" {
  default = 90
}

variable "cpu_utilization_unit" {
  default = ""
}

// Disk Space Utilization
variable "disk_space_utilization_comparison_operator" {
  default = "GreaterThanOrEqualToThreshold"
}

variable "disk_space_utilization_evaluation_periods" {
  default = 3
}

variable "disk_space_utilization_period" {
  default = 300
}

variable "disk_space_utilization_statistic" {
  default = "Average"
}

variable "disk_space_utilization_threshold" {
  default = 90
}

variable "disk_space_utilization_unit" {
  default = "Percent"
}

// Memory Utilization
variable "memory_utilization_comparison_operator" {
  default = "GreaterThanOrEqualToThreshold"
}

variable "memory_utilization_evaluation_periods" {
  default = 3
}

variable "memory_utilization_period" {
  default = 300
}

variable "memory_utilization_statistic" {
  default = "Average"
}

variable "memory_utilization_threshold" {
  default = 90
}

variable "memory_utilization_unit" {
  default = "Percent"
}


// Status Check
variable "status_check_comparison_operator" {
  default = "GreaterThanThreshold"
}

variable "status_check_evaluation_periods" {
  default = 1
}

variable "status_check_period" {
  default = 180
}

variable "status_check_statistic" {
  default = "Maximum"
}

variable "status_check_threshold" {
  default = 0
}

variable "status_check_unit" {
  default = "Count"
}
