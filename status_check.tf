# Status Check
resource "aws_cloudwatch_metric_alarm" "status_check" {
  alarm_name          = "[${var.account_name}] [ec2] [${var.env}] ${var.instance_name} - Status Check"
  comparison_operator = "${var.status_check_comparison_operator}"
  evaluation_periods  = "${var.status_check_evaluation_periods}"
  metric_name         = "StatusCheckFailed"
  namespace           = "AWS/EC2"
  period              = "${var.status_check_period}"
  statistic           = "${var.status_check_statistic}"
  threshold           = "${var.status_check_threshold}"
  unit                = "${var.status_check_unit}"
  alarm_actions       = ["${var.alarm_actions}"]
  dimensions {
    InstanceId = "${var.instance_id}"
  }
}

output "status_check" {
  value = {
    id = "${aws_cloudwatch_metric_alarm.status_check.id}"
  }
}
