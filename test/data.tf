data "aws_region" "current" {
  current = true
}

// Get the AZs from Amazon
data "aws_availability_zones" "azs" { }

/* AWS account
 * Declare this on TF_VAR_account environment variable with your AWS account id
 * for the test account
 */
variable account {}

data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
  filter {
    name = "image-id"
    values = ["ami-7fa00169"]
  }
}
