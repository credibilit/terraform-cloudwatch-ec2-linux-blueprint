// VPC
module "test_vpc" {
  source  = "git::https://bitbucket.org/credibilit/terraform-vpc-blueprint.git?ref=0.0.8"
  account = "${var.account}"

  name                      = "cloudwatch_test"
  domain_name               = "cloudwatch_test.local"
  cidr_block                = "10.0.0.0/16"
  azs                       = "${data.aws_availability_zones.azs.names}"
  az_count                  = 4
  hosted_zone_comment       = "An internal hosted zone for testing"
  public_subnets_cidr_block = [
    "10.0.0.0/24",
    "10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24"
  ]
}

resource "aws_instance" "web" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"
  subnet_id     = "${element(module.test_vpc.public_subnets, 0)}"
  tags {
    Name = "Test_CloudWatch"
    CSC_MON = ""
    CSC_SEG = ""
  }
}

// Action
resource "aws_sns_topic" "monitoring" {
  name         = "monitoring"
  display_name = "monitoring"
}

module "test_ec2_cloudwatch_web" {
  source  = "../"
  account = "${var.account}"

  account_name  = "credibilit-tst"
  env           = "tst"
  instance_id   = "${aws_instance.web.id}"
  instance_name = "Test_CloudWatch"
  alarm_actions = ["${aws_sns_topic.monitoring.arn}"]
  devices       = "${map(
    "dev", list("/dev/sda1"),
    "mount", list("/")
  )}"
}
