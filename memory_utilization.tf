# Memory Utilization
resource "aws_cloudwatch_metric_alarm" "memory_utilization" {
  alarm_name          = "[${var.account_name}] [ec2] [${var.env}] ${var.instance_name} - Memory Utilization"
  comparison_operator = "${var.memory_utilization_comparison_operator}"
  evaluation_periods  = "${var.memory_utilization_evaluation_periods}"
  metric_name         = "MemoryUtilization"
  namespace           = "System/Linux"
  period              = "${var.memory_utilization_period}"
  statistic           = "${var.memory_utilization_statistic}"
  threshold           = "${var.memory_utilization_threshold}"
  unit                = "${var.memory_utilization_unit}"
  alarm_actions       = ["${var.alarm_actions}"]
  dimensions {
    InstanceId = "${var.instance_id}"
  }
}

output "memory_utilization" {
  value = {
    id = "${aws_cloudwatch_metric_alarm.memory_utilization.id}"
  }
}
